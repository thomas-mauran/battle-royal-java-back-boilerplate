package com.nmeo.game;

import com.nmeo.models.Bullet;
import com.nmeo.models.GameStatus;
import com.nmeo.models.Player;

import java.util.*;

public class GameSession {
    private UUID gameUUID;
    private String gameName;
    private Set<Player> players;

    private ArrayList<Bullet> bullet;
    private Optional<String> winnerName;

    // TODO
    public void createPlayer(Player player){
    }

    // TODO
    public void deletePlayer(Player player){}

    // TODO
    public void updatePlayer(Player player){}

    // TODO
    public void newBullet(Bullet bullet){}

    // TODO
    public void destroyBullet(Bullet bullet){}

    // TODO
    public Set<Player> getOtherPlayers(UUID uuid){
        return new HashSet<>();
    }

    // TODO
    public Set<Player> getPlayers(){
        return new HashSet<>();
    }

    // TODO
    public Optional<String> getWinnerName(){
        return Optional.empty();
    }

    // TODO
    public GameStatus getGameStatus(){
        return GameStatus.LOBBY;
    }

    // TODO
    public void setGameStatus(GameStatus status){}

    // TODO
    private void isGameFinished(){}



}
