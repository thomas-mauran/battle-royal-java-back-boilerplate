package com.nmeo.interfaces;

import com.nmeo.game.GameSession;
import com.nmeo.models.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

public interface IGameManager {

    void createGame(UUID uuid, String name);

    GameState createPlayer(UUID uuid, Player player);

    GameState destroyPlayer(UUID uuid, Player player);
    GameState updatePlayer(UUID uuid, Player player);

    void createBullet(UUID uuid, Bullet bullet);

    void deleteBullet(UUID uuid, Bullet bullet);

    GameState setGameState(UUID uuid, GameStatus gameStatus);

    ArrayList<Game> getGameList();


}
