package com.nmeo.models;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class Bullet implements Comparable<Bullet>{
    public UUID uuid;
    public int startX;
    public int toX;
    public int toY;

    @Override
    public int compareTo(@NotNull Bullet bullet) {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
