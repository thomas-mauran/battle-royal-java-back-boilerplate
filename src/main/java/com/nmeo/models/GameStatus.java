package com.nmeo.models;

public enum GameStatus {
    IDLE(0),
    LOBBY(1),
    STARTING(2),
    PLAYING(3),
    FINISHED(4);

    public final Integer type;

    private GameStatus(Integer m_type) {this.type = m_type;}

}
