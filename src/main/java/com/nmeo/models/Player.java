package com.nmeo.models;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class Player implements Comparable<Player>{
    public UUID uuid;
    public String name;
    public int x;
    public int y;
    public int velocityX;
    public int velocityY;
    public int direction;
    public String atlas;
    public String frame;
    public int width;
    public int height;
    public boolean isAlive;

    public Player(String name, int x, int y, int velocityX, int velocityY, int direction, String atlas, String frame, int width, int height, boolean isAlive) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        this.direction = direction;
        this.atlas = atlas;
        this.frame = frame;
        this.width = width;
        this.height = height;
        this.isAlive = isAlive;
    }

    @Override
    public int compareTo(@NotNull Player player) {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
