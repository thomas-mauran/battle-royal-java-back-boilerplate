package com.nmeo.models;

import java.util.Optional;
import java.util.Set;

public class GameState {
    public Set<Player> player;

    public GameStatus gameStatus;
    public Optional<String> winnerName;
}
